const promiseAwait = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve("Hi!");
    }, 3000);
});

(async () => {
    const result = await promiseAwait;
    console.log(result);
})();