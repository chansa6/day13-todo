import { Button, Modal, Input } from "antd"
import { EditOutlined, CloseOutlined } from '@ant-design/icons';
import { useTodos } from "./hooks/useTodos";
import { useState } from "react";
import "./TodoItem.css";

const TodoItem = ({ todoItem }) => {

    const { toggleTodo, deleteTodo, editTodo } = useTodos();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [editText, setEditText] = useState(todoItem.text);
    const { TextArea } = Input;

    const handleToggleItem = () => {
        toggleTodo(todoItem.id, !todoItem.done);
    };

    const handleDeleteItem = (event) => {
        event.stopPropagation();
        if (window.confirm("Are you sure you want to delete this todo item?")) {
            deleteTodo(todoItem.id);
        }
    };

    const handleEditItem = (event) => {
        event.stopPropagation();
        if (editText !== null) {
            editTodo(
                todoItem.id,
                editText
            );
        }
        setIsModalOpen(false);
    };

    const showModal = (event) => {
        event.stopPropagation();
        setIsModalOpen(true);
    };

    const handleCancel = (event) => {
        event.stopPropagation();
        setIsModalOpen(false);
    };

    return (
        <div className="todo-item">
            <div className="todo-content">
                <div onClick={handleToggleItem}>
                    <span className={todoItem.done ? "done" : ""}>
                        {todoItem.text}
                    </span>
                </div>
                <div className="button-container">
                    <div onClick={handleDeleteItem}>
                        <Button className="delete-button">
                            <CloseOutlined />
                        </Button>
                    </div>
                    <div onClick={showModal}>
                        <Button className="edit-button" >
                            <EditOutlined />
                        </Button>
                    </div>
                </div>
            </div>
            <Modal title="TodoItem"
                open={isModalOpen}
                onOk={handleEditItem}
                onCancel={handleCancel}
                okButtonProps={{ disabled: !editText.trim() }}>
                <TextArea value={editText}
                    onChange={e => setEditText(e.target.value)} />
            </Modal>
        </div>
    )
}

export default TodoItem;
