import "./TodoItem.css";
import { Link } from "react-router-dom";

const DoneItem = ({ todoItem }) => {
    return (
        <div>
            <Link to={`/todos/${todoItem.id}`}>
                {todoItem.text}
            </Link>
        </div>
    )
}

export default DoneItem;
