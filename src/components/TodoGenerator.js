import { useState } from "react";
import { useTodos } from "./hooks/useTodos";
import { Button } from "antd"

function TodoGenerator() {

    const [text, setText] = useState("")
    const { addTodo } = useTodos();

    const handleInputChange = (event) => {
        setText(event.target.value)
    }

    const handleAddItem = () => {
        addTodo({
            text: text,
            done: false
        });
        setText("");
    }

    return (
        <div>
            <input type="text" value={text} onChange={handleInputChange} />
            <Button type="primary" onClick={handleAddItem}
            disabled={!text.trim()}>Add</Button>
        </div>
    )
}

export default TodoGenerator;