import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

const TodoDetail = () => {
    const { id } = useParams();

    const todo = useSelector(
        (state) => state.todo.todoItems
        .find((todoItem) => todoItem.id === id))


    return (
        <div>
            <h1>Todo Detail</h1>
            <div>ID: {todo.id}</div>
            <div> Content: {todo.text} </div>
        </div>
    );
}

export default TodoDetail;