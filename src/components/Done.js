import { useSelector } from "react-redux";
import { createSelector } from '@reduxjs/toolkit';
import DoneItem from "./DoneItem";
import "./Done.css";

const selectDoneItems = createSelector(
    state => state.todo.todoItems,
    todoItems => todoItems.filter(todoItem => todoItem.done)
);

const Done = () => {
    const doneItems = useSelector(selectDoneItems);
    
    return (
        <div className="todo-done-container">
            <h1>Done List</h1>
            {doneItems.map((doneItem) => (
                <DoneItem key={doneItem.id} todoItem={doneItem} />
            ))}
        </div>
    )
}

export default Done;