import { useSelector } from "react-redux";
import TodoItem from "./TodoItem";
import { useEffect } from "react";
import { useTodos } from "./hooks/useTodos";
import "./TodoItem.css";
import "./TodoGroup.css";

const TodoGroup = () => {

    const todoItems = useSelector((state) => state.todo.todoItems);
    const { loadTodos } = useTodos();

    useEffect(() => {
        loadTodos();
    }, []);

    return (
        <div className="todo-group-container">
            {todoItems.map((todoItem) => (
                <TodoItem key={todoItem.id} todoItem={todoItem} />
            ))}
        </div>
    );
};

export default TodoGroup;