import { useDispatch } from "react-redux";
import { getTodos, postTodoAPI, updateTodoAPI, deleteTodoAPI } from "../../api/todos";
import { addTodos } from "../../app/todoSlice";

export const useTodos = () => {
    const dispatch = useDispatch();

    const loadTodos = () => {
        getTodos().then((response) => {
            dispatch(addTodos(response.data));
        });
    };

    const addTodo = async (newTodo) => {
        await postTodoAPI(newTodo);
        loadTodos();
    };

    const toggleTodo = async (id, done) => {
        await updateTodoAPI(id, {
            done: done,
        });
        loadTodos();
    };

    const deleteTodo = async (id) => {
        await deleteTodoAPI(id);
        loadTodos();
    };

    const editTodo = async (id, text) => {
        await updateTodoAPI(id, {
            text: text,
        });
        loadTodos();
    }

    return {
        loadTodos,
        addTodo,
        toggleTodo,
        deleteTodo,
        editTodo,
    }
}

