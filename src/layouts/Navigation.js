import { Link } from "react-router-dom";
import "./Navigation.css";

const Navigation = () => {
    return (
        <nav className="nav-section">
            <ul className="nav-list">
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/done">Done</Link>
                </li>
                <li>
                    <Link to="/about">About</Link>
                </li>
            </ul>
        </nav>
    );
}

export default Navigation;