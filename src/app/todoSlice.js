import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    todoItems: [],
};

const todoSlice = createSlice({
    name: "todo",
    initialState,
    reducers: {
        addTodo: (state, action) => {
            state.todoItems.push(action.payload);
        },
        deleteTodo: (state, action) => {
            state.todoItems = state.todoItems.filter(
                (todoItem) => todoItem.id !== action.payload
            );
        },
        changeCompleteStatus: (state, action) => {
            var foundIndex = state.todoItems.findIndex(
                (arrItem) => arrItem.id === action.payload
            );
            state.todoItems[foundIndex].done =
                !state.todoItems[foundIndex].done;
        },
        addTodos: (state, action) => {
            state.todoItems = action.payload;
        },

    }
});

export const { addTodo, deleteTodo, changeCompleteStatus, addTodos } = todoSlice.actions;
export default todoSlice.reducer;