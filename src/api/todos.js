import client from "./client";

export const getTodos = () => {
    return client.get("/todos");
}

export const postTodoAPI = (newTodo) => {
    return client.post("/todos", newTodo);
}

export const updateTodoAPI = (id, requestBody) => {
    return client.put("todos/" + id, requestBody);
}

export const deleteTodoAPI = (id) => {
    return client.delete("todos/" + id);
}