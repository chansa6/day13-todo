import axios from "axios";

const client = axios.create({
    baseURL: "https://65664191eb8bb4b70ef3196d.mockapi.io/",
});

export default client;