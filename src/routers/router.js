import { createBrowserRouter } from "react-router-dom";
import AboutPage from "../pages/AboutPage";
import Done from "../components/Done";
import Layout from "../layouts/Layout";
import TodoList from "../components/TodoList";
import TodoDetail from "../components/TodoDetail";
import NotFoundPage from "../pages/NotFoundPage";

const router = createBrowserRouter(
    [
        {
            path: "/",
            element: <Layout />,
            errorElement: <NotFoundPage />,
            children: [
                {
                    index: true,
                    path: "/",
                    element: <TodoList />
                },
                {
                    path: "/todo",
                    element: <div>hello todo</div>
                },
                {
                    path: "/done",
                    element: <Done />
                },
                {
                    path: "/about",
                    element: <AboutPage />
                },
                {
                    path: "/todos/:id",
                    element: <TodoDetail />
                },
            ]
        },
    ]
);

export default router;